import {Component, Input} from "@angular/core";

@Component({
  selector: "app-movie",
  templateUrl: "./movie.components.html",
  styleUrls: ["./movie.components.css"]
})


export class MovieComponents {
  @Input() title = 'Побег из Шоушенка';
  @Input() year = 1994;
  @Input() url = 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid%3DOIP.7ZfW75__ihWk_LDMb02SfwHaKl%26pid%3DApi&f=1'
}
